package golem

import (
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/bmatcuk/doublestar"
)

// Command returns a Cmd that has not been run yet. You will need to call
// Run() on the command to actually start the child process.
func (c *context) Command(cmd string, args ...string) Cmd {
	return Command(cmd, args...)
}

// Exec runs a command immediately.
func (c *context) Exec(cmd string, args ...string) {
	Command(cmd, args...).Run()
}

// Find returns an array of filenames that match the glob pattern.
func (c *context) Find(pattern string) []string {
	if m, err := doublestar.Glob(pattern); err != nil {
		panic(err)
	} else {
		return m
	}
}

// Visit executes a visitor function for each filename that matches the glob
// pattern.
func (c *context) Visit(pattern string, visit func(string)) {
	for _, f := range c.Find(pattern) {
		visit(f)
	}
}

// Exists returns true if any files match the glob pattern.
func (c *context) Exists(pattern string) bool {
	return c.Find(pattern) != nil
}

// Delete deletes any files or directories that match the glob pattern.
func (c *context) Delete(pattern string) {
	files := c.Find(pattern)

	for _, f := range files {
		if abs, err := filepath.Abs(f); err != nil {
			panic(err)
		} else if strings.HasSuffix(abs, "/.golem") || strings.HasSuffix(abs, "\\.golem") || strings.Contains(abs, "/.golem/") || strings.Contains(abs, "\\.golem\\") {
			continue
		} else if err := os.RemoveAll(f); err != nil {
			panic(err)
		}
	}
}

// EnsureDir creates a directory, including any missing parent directories. It
// does nothing if the directory already exists.
func (c *context) EnsureDir(dir string) {
	if dir == "." || dir == "" {
		return
	}

	if err := os.MkdirAll(dir, 0755); err != nil {
		panic(err)
	}
}

// Copy the source file to the target file. This only works on files, not
// directories.
func (c *context) Copy(source, target string) {
	if in, err := os.Open(source); err != nil {
		panic(err)
	} else {
		defer in.Close()
		c.EnsureDir(filepath.Dir(target))
		if out, err := os.OpenFile(target, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644); err != nil {
			panic(err)
		} else {
			defer out.Close()
			if _, err := io.Copy(out, in); err != nil {
				panic(err)
			}
		}
	}
}

// Rename the source file or directory to the target name.
func (c *context) Rename(source, target string) {
	if err := os.Rename(source, target); err != nil {
		panic(err)
	}
}

// Write a file. If the file exists, it will be overwritten. If it does not
// exist, it will be created.
func (c *context) Write(filename string, b []byte) {
	c.EnsureDir(filepath.Dir(filename))

	if out, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644); err != nil {
		panic(err)
	} else {
		defer out.Close()
		if _, err := out.Write(b); err != nil {
			panic(err)
		}
	}
}

// Append data to a file. If the file exists, the bytes are added to the end.
// If it does not exist, it will be created.
func (c *context) Append(filename string, b []byte) {
	c.EnsureDir(filepath.Dir(filename))

	if out, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0644); err != nil {
		panic(err)
	} else {
		defer out.Close()
		if _, err := out.Write(b); err != nil {
			panic(err)
		}
	}
}

// Dir gets the current working directory.
func (c *context) Dir() string {
	if v, err := os.Getwd(); err != nil {
		panic(err)
	} else {
		return v
	}
}

// SetDir sets the current working directory.
func (c *context) SetDir(dir string) {
	if err := os.Chdir(dir); err != nil {
		panic(err)
	}
}

// GoUpdate is shorthand for `c.Exec("go", "add", "-u", "all")``. It also
// temporarily restores GOOS and GOARCH to the current OS and architecture.
func (c *context) GoUpdate() {
	cmd := c.Command("go", "get", "-u", "all")
	cmd.Env("GOOS", "")
	cmd.Env("GOARCH", "")
	cmd.Run()
}

// GoTidy is shorthand for `c.Exec("go", "mod", "tidy")``.
func (c *context) GoTidy() {
	c.Exec("go", "mod", "tidy")
}
