package golem

import "gitlab.com/ottersoft/golem/semver"

// Publish does the following:
//   - Checks that the working directory is clean, and fails if it's not.
//   - Pushes the current branch to origin.
//   - Creates an annotated tag for the given version, at the current commit.
//   - Pushes the tag to origin.
func (c *context) Publish(v semver.Semver) {
	c.GitAssertClean()
	c.Exec("git", "push", "origin", c.GitCurrentBranch())
	c.GitTagVersion(v)
	c.Exec("git", "push", "origin", v.String())
}
