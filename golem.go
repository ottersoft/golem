// Package golem provides simple Go build scripting.
package golem

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/ottersoft/golem/internal"
	"gitlab.com/ottersoft/golem/semver"
)

// Golem is the main Golem build scripting engine.
type Golem interface {
	AddTask(name string, callback func(Context), options ...string)
	HasTask(string) bool
	RunTask(name string, args ...string)
	Run(args []string)
}

type golem struct {
	tasks map[string]TaskSpec
	done  map[string]bool
}

// New creates a new Golem instance.
func New() Golem {
	return &golem{map[string]TaskSpec{}, map[string]bool{}}
}

var g = New()

// Add a task. Panics if the task name already exists.
func (g *golem) AddTask(name string, fn func(Context), options ...string) {
	if _, exists := g.tasks[name]; exists {
		panic(fmt.Errorf("'%s' task is already defined", name))
	}

	g.tasks[name] = TaskSpec{fn, options}
}

// HasTask returns true if the task name has been added.
func (g *golem) HasTask(name string) bool {
	_, ok := g.tasks[name]
	return ok
}

// RunTask runs the named task with the given args. Panics if the task name
// does not exist.
func (g *golem) RunTask(name string, args ...string) {
	if done, started := g.done[name]; started && !done {
		panic(fmt.Errorf("'%s' task is circular", name))
	} else if task, ok := g.tasks[name]; !ok {
		panic(fmt.Errorf("'%s' task is not defined", name))
	} else {
		o := ParseOptions(name, task.options)

		if len(args) > 0 && (args[0] == "--help" || args[0] == "-help") {
			o(args)
			return
		}

		Print(os.Stderr, Style(":%s", ColorBlue), name)
		g.done[name] = false

		c := NewContext(g, name, args, ParseOptions(name, task.options))

		task.Run(c)

		if c.Error() != nil {
			panic(c.Error())
		}

		g.done[name] = true
	}
}

// Run the task indicated by the command line args. The first argument should
// be the task name, followed by arguments consumed by that task.
func (g *golem) Run(args []string) {
	checkVersion()

	if len(args) < 1 || args[0] == "--help" || args[0] == "-help" {
		Print(os.Stderr, strings.TrimSpace(`
Usage: golem <task> [options...]
       golem <task> -help
       golem -version
       golem -help
		`)+"\n")

		Print(os.Stderr, "Tasks:")
		for k := range g.tasks {
			if k != "init" && k != "upgrade" && k != "sync" {
				Print(os.Stderr, "  "+k)
			}
		}
		Print(os.Stderr, "  init")
		Print(os.Stderr, "  upgrade")
		if IsGolem() {
			Print(os.Stderr, "  sync")
		}
		Print(os.Stderr, "\nOptions:")
		Print(os.Stderr, "  -version\n        Print the Golem CLI version.")
		Print(os.Stderr, "  -help\n        Print this usage text.")

		return
	}

	task := args[0]
	args = args[1:]
	help := len(args) > 0 && (args[0] == "--help" || args[0] == "-help")

	if !help {
		t := time.Now()
		defer func() {
			Print(os.Stderr, Style("Elapsed time: %s", ColorGray), time.Now().Sub(t).Truncate(time.Millisecond))
		}()
	}

	defer func() {
		if err := recover(); err != nil {
			Print(os.Stderr, Style("%s", ColorRed), err)
			Print(os.Stderr, Style("\nBUILD FAILED\n", ColorRed))
			os.Exit(1)
		}
	}()

	g.RunTask(task, args...)

	if !help {
		Print(os.Stderr, Style("\nBUILD SUCCESSFUL\n", ColorGreen))
	}
}

// Task adds a task to the global Golem singleton instance.
//
// Options strings are "usage-like" strings which generate the task usage
// text and options.
//
// An option string may start with a hyphen (-, two are allowed), followed by
// a flag name (alpha-numeric + hyphens), followed by 1 or more spaces,
// followed by an optional type indicator (i.e <bool>, <int>, <float>, or
// <string>), followed by 1 or more spaces, and finally a flag description
// string. If the type indicator is omitted, it defaults to <bool>. If the
// option string matches this format, then the name, type, and description
// will be parsed and used to generate an option (i.e. a Go flag.FlagSet
// option). The values of these options can be accessed from the task context
// (i.e. the golem.Context.Option() method)
//
// Options strings which do not start with a hyphen, will be printed as
// paragraphs in the task usage text.
//
// Examples:
//   "This is a paragraph in the usage text"
//   "-myFlag  <string> This is a flag which accepts a string value."
//   "-myInt   <int>    This is a flag which accepts an integer value."
//   "-myFloat <float>  This is a flag which accepts a floating point value."
//   "-myBool  <bool>   This is a boolean flag which takes no value."
//   "-alsoBool         This is a also a boolean flag which takes no value."
func Task(name string, callback func(Context), options ...string) {
	g.AddTask(name, callback, options...)
}

// Run runs the global Golem singleton instance. The args will be os.Args[1:].
func Run() {
	g.Run(os.Args[1:])
}

// IsGolem returns true if the .golem directory exists in the working
// directory.
func IsGolem() (ok bool) {
	// Ignore errors
	defer func() { recover() }()

	if s, err := os.Stat(".golem"); err == nil && s.IsDir() {
		ok = true
	}

	return
}

func checkVersion() {
	// Ignore errors
	defer func() { recover() }()

	cli, _ := semver.Parse(Command("golem", "-version").OutputString())
	mod := semver.MustParse(internal.Version)
	eq := cli.Compare(mod)

	if eq != 0 {
		Print(os.Stderr, Style("Golem versions are out of sync (cli=%s, module=%s)!", ColorYellow), cli, mod)
		Print(os.Stderr, Style("  Run `golem upgrade` to install the latest version of the CLI (optional).", ColorYellow))
		Print(os.Stderr, Style("  Run `golem sync` to set the module version (.golem/go.mod) to the CLI version.", ColorYellow))
		Print(os.Stderr, "")
	}
}
