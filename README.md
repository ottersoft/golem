# Golem

Simple Go build scripting.

## Getting Started

Install the golem binary.

```sh
go get gitlab.com/ottersoft/golem/cmd/golem
```

Navigate to the root of your go project, and run the `golem init` task. This will create the following file structure:

- `.golem/`
  - `go.mod`
  - `golem.go`

The `.golem/golem.go` file contains a simple build script template. Update it to include your own build tasks.

Run `golem` without a task to see a list of available tasks.
