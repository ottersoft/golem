// +build golem

package main

import (
	"fmt"

	"gitlab.com/ottersoft/golem"
	"gitlab.com/ottersoft/golem/semver"
)

func main() {
	golem.Task("build", func(c golem.Context) {
		c.WriteBuildValues("internal/version.go", false, func(b *golem.BuildValues) {
			b.Import("gitlab.com/ottersoft/golem/semver")
			b.Var("Version", c.GitVersionTags(1).Latest(), "at build time.")
		})

		c.RunTask("test")
		c.Exec("go", "mod", "tidy")
		c.EnsureDir("out/bin")
		c.Exec("go", "build", "-o=out/bin", "./...")
	})

	golem.Task("cover", func(c golem.Context) {
		c.RunTask("test")
		c.Exec("go", "tool", "cover", "-html=out/tmp/cp.out")
	})

	golem.Task("test", func(c golem.Context) {
		c.EnsureDir("out/tmp")
		c.Exec("go", "test", "-coverprofile", "out/tmp/cp.out", "./...")
		c.Exec("go", "tool", "cover", "-func=out/tmp/cp.out")
	})

	golem.Task("clean", func(c golem.Context) {
		c.Delete("**/out/")
	})

	golem.Task("publish", func(c golem.Context) {
		c.GitAssertClean()

		var v semver.Semver
		ver := c.Option("version").String()

		if ver == "major" {
			v = c.GitVersionTags(1).Latest().BumpMajor()
		} else if ver == "minor" {
			v = c.GitVersionTags(1).Latest().BumpMinor()
		} else if ver == "" {
			v = c.GitVersionTags(1).Latest().Bump()
		} else {
			v = semver.MustParse(ver)
		}

		c.WriteBuildValues("internal/version.go", true, func(b *golem.BuildValues) {
			b.Var("Version", v.String(), "at build time.")
		})

		c.RunTask("build")
		if !c.GitChanges().Empty() {
			c.GitCommit(fmt.Sprintf("Build output for version %s", v), true)
		}

		c.Log("New version: %s", v)
		c.Publish(v)
	},
		"-version <string> Set the version to be published. Can also be 'major' or 'minor'.",
	)

	golem.Run()
}
