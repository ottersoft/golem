package golem

// Context is the task context of a Golem task. It contains information about
// the task, and utility methods.
type Context struct {
	*context
}

type context struct {
	name             string
	err              error
	args             []string
	golem            Golem
	optionSetFactory OptionSetFactory
	optionSet        *OptionSet
}

// NewContext creates a new Golem task context.
func NewContext(g Golem, name string, args []string, optionSetFactory OptionSetFactory) Context {
	return Context{&context{golem: g, name: name, args: args, optionSetFactory: optionSetFactory}}
}

// Name returns the name of the task.
func (c *context) Name() string {
	return c.name
}

// Error returns an error set on the task, or nil.
func (c *context) Error() error {
	return c.err
}

// SetError sets an error on the task. The task will continue running, but
// the Golum build will fail after the task completes. If you want to fail
// immediately, use panic().
func (c *context) SetError(err error) {
	c.err = err
}

// Option gets a task option value.
func (c *context) Option(name string) Option {
	if c.optionSet == nil {
		o := c.optionSetFactory(c.args)
		c.optionSet = &o
	}

	return c.optionSet.Option(name)
}

// Args gets the command line arguments passed to the task.
func (c *context) Args() []string {
	return c.args
}

// HasTask returns true if the task name is defined on the running Golem
// instance.
func (c *context) HasTask(name string) bool {
	return c.golem.HasTask(name)
}

// RunTask runs another task on the Golem instance that ran this task.
func (c *context) RunTask(name string, args ...string) {
	c.golem.RunTask(name, args...)
}
