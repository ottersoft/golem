package golem

import (
	"regexp"
	"strings"
)

var multiSpacesRx = regexp.MustCompile(" {2,}")

func removeLineBreaks(s string) string {
	s = strings.ReplaceAll(s, "\r\n", " ")
	s = strings.ReplaceAll(s, "\n", " ")
	return s
}

func removeMultiSpaces(s string) string {
	s = multiSpacesRx.ReplaceAllString(s, " ")
	return s
}

func wrap(s string, w int) string {
	if w < 1 {
		w = 1
	}

	s = strings.ReplaceAll(s, "\t", "    ")

	var b strings.Builder

	for len(s) > w {
		s1 := s[0 : w+1]

		if i := strings.IndexAny(s1, "\n"); i >= 0 {
			b.WriteString(s1[0 : i+1])
			s = s[i+1:]
		} else if i := strings.LastIndex(s1, " "); i >= 0 {
			b.WriteString(s1[0 : i+1])
			b.WriteString("\n")
			s = s[i+1:]
		} else {
			b.WriteString(s1[0:w])
			b.WriteString("\n")
			s = s[w:]
		}
	}

	if len(s) > 0 {
		b.WriteString(s)
	}

	return b.String()
}
