package golem

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"syscall"
)

var lineBreakRx = regexp.MustCompile(`(\n|\r\n?)`)

// Cmd is an un-executed command. Use the Run() method to execute the command.
type Cmd struct {
	*exec.Cmd
	cmd string
}

func (c Cmd) String() string {
	return c.cmd
}

// Dir sets the working directory of the command. Defaults to the current
// working directory.
func (c Cmd) Dir(dir string) Cmd {
	c.Cmd.Dir = dir
	return c
}

// Env sets an environment variable.
func (c Cmd) Env(name string, value string) Cmd {
	if c.Cmd.Env == nil {
		c.Cmd.Env = os.Environ()
	}
	c.Cmd.Env = append(c.Cmd.Env, fmt.Sprintf("%s=%s", name, value))
	return c
}

// Stdout sets the stdout stream of the command. Defaults to os.Stdout.
func (c Cmd) Stdout(w io.Writer) Cmd {
	c.Cmd.Stdout = w
	return c
}

// Stderr sets the stderr stream of the command. Defaults to os.Stderr.
func (c Cmd) Stderr(w io.Writer) Cmd {
	c.Cmd.Stderr = w
	return c
}

// Stdin sets the stderr stream of the command. Defaults to os.Stdin.
func (c Cmd) Stdin(r io.Reader) Cmd {
	c.Cmd.Stdin = r
	return c
}

// Run the command, and wait until it completes.
func (c Cmd) Run() {
	c.Cmd.Stdin = os.Stdin
	c.Cmd.Stdout = os.Stdout
	c.Cmd.Stderr = os.Stderr
	err := c.Cmd.Run()
	c.handleError(err)
}

// Start the command, and return immediately (non-blocking). Use Wait() to
// block until the command completes.
func (c Cmd) Start() {
	err := c.Cmd.Start()
	c.handleError(err)
}

// Wait for a started command to complete.
func (c Cmd) Wait() {
	err := c.Cmd.Wait()
	c.handleError(err)
}

// Output runs the command, waits for it to complete, then returns bytes
// written to the stdout stream.
func (c Cmd) Output() []byte {
	c.Cmd.Stdin = os.Stdin
	c.Cmd.Stderr = os.Stderr
	b, err := c.Cmd.Output()
	c.handleError(err)
	return b
}

// OutputString runs the command, waits for it to complete, then returns bytes
// written to the stdout stream as a string.
func (c Cmd) OutputString() string {
	return string(c.Output())
}

// OutputLines runs the command, waits for it to complete, then returns all
// lines written to the stdout stream. Lines can be delineated by "\n" (unix)
// or "\r\n" (windows).
func (c Cmd) OutputLines() []string {
	l := lineBreakRx.Split(strings.TrimSpace(c.OutputString()), -1)

	if len(l) == 1 && l[0] == "" {
		l = []string{}
	}

	return l
}

// CombinedOutput runs the command, waits for it to complete, then returns
// bytes written to the stdout and stderr streams.
func (c Cmd) CombinedOutput() []byte {
	c.Cmd.Stdin = os.Stdin
	b, err := c.Cmd.CombinedOutput()
	c.handleError(err)
	return b
}

// CombinedOutputString runs the command, waits for it to complete, then
// returns bytes written to the stdout and stderr streams as a string.
func (c Cmd) CombinedOutputString() string {
	return string(c.CombinedOutput())
}

// CombinedOutputLines runs the command, waits for it to complete, then
// returns all lines written to the stdout and stderr streams. Lines can be
// delineated by "\n" (unix) or "\r\n" (windows).
func (c Cmd) CombinedOutputLines() []string {
	return lineBreakRx.Split(strings.TrimSpace(c.CombinedOutputString()), -1)
}

func (c Cmd) handleError(err interface{}) {
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				panic(fmt.Errorf("Exec(%v) exit status: %d", c.cmd, status.ExitStatus()))
			}
		}

		panic(fmt.Errorf("Exec(%v) error: %v", c.cmd, err))
	}
}

// Command creates a new Cmd, but does not run it immediately.
func Command(cmd string, args ...string) Cmd {
	return Cmd{exec.Command(cmd, args...), commandString(cmd, args)}
}

func commandString(cmd string, args []string) string {
	b := strings.Builder{}
	b.WriteString(fmt.Sprintf("\"%s\"", cmd))
	for _, a := range args {
		b.WriteString(fmt.Sprintf(", \"%s\"", a))
	}
	return b.String()
}
