package golem

import (
	"fmt"
	"strings"

	"gitlab.com/ottersoft/golem/semver"
)

// GitVersionTags is an array of semver.Semver instances, with utility methods.
type GitVersionTags []semver.Semver

// GitChanges is a string array of Git changes, with utility methods.
type GitChanges []string

// GitChanges returns all modifications to the working directory (including
// unstaged).
func (c *context) GitChanges() GitChanges {
	return GitChanges(c.Command("git", "status", "--porcelain").OutputLines())
}

// GitAssertClean causes the task to fail immediately if the working directory
// is not clean.
func (c *context) GitAssertClean() {
	if !c.GitChanges().Empty() {
		panic(fmt.Errorf("working directory is not clean"))
	}
}

// Empty returns true if the Git changes array is empty.
func (chs GitChanges) Empty() bool {
	return len(chs) == 0
}

// GitVersionTags returns all version tags, sorted in descending order, up to
// a maximum of limit versions.
func (c *context) GitVersionTags(limit int) (l1 GitVersionTags) {
	l := c.Command("git", "tag", "-l", "v*", "--sort=-version:refname").OutputLines()

	for i, s := range l {
		if limit > 0 && i >= limit {
			break
		}

		v, ok := semver.Parse(s)

		if ok {
			l1 = append(l1, v)
		}
	}

	return
}

// Latest returns the zero entry in the version tag array, or a zero (v0.0.0)
// version if the array is empty.
func (vs GitVersionTags) Latest() (v semver.Semver) {
	if len(vs) > 0 {
		v = vs[0]
	}
	return
}

// Commit all modified files (staged or not). Also add untracked files if
// addAll is true.
func (c *context) GitCommit(message string, addAll bool) {
	if addAll {
		c.Exec("git", "add", "-A")
	}

	c.Exec("git", "commit", "-a", "-m", message)
}

// GitTag adds an annotated Git tag.
func (c *context) GitTag(tag string, message string) {
	c.Exec("git", "tag", "-a", tag, "-m", message)
}

// GitTagVersion adds an annotated version tag.
func (c *context) GitTagVersion(v semver.Semver) {
	c.GitTag(v.String(), fmt.Sprintf("Publishing version %s", v))
}

// GitCurrentBranch returns the current branch name.
func (c *context) GitCurrentBranch() string {
	return strings.TrimSpace(c.Command("git", "branch", "--show-current").OutputString())
}
