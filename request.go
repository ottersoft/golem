package golem

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

// ResponseCode is an int that is an HTTP status code.
type ResponseCode int

// AssertSuccessResponseCode panics if the response code is non-2xx.
func (r ResponseCode) AssertSuccessResponseCode() {
	if r/100 != 2 {
		panic(fmt.Errorf("non-2xx response received: %d", r))
	}
}

// Request makes an HTTP request. All errors cause panics. Non-2xx response
// codes DO NOT cause panics. The response content is returned as bytes and
// the response code is returned as an int.
func (c *context) Request(method string, url string, body io.Reader) ([]byte, ResponseCode) {
	req, err := http.NewRequest(strings.ToUpper(method), url, body)

	if err != nil {
		panic(err)
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err)
	}

	return b, ResponseCode(res.StatusCode)
}

// RequestJSON calls Request(), and then unmarshals the body if the response
// code is 2xx. Any errors during unmarshalling will cause a panic.
func (c *context) RequestJSON(method string, url string, body io.Reader, value interface{}) ResponseCode {
	b, s := c.Request(method, url, body)

	if s >= 200 && s < 300 {
		err := json.Unmarshal(b, value)

		if err != nil {
			panic(err)
		}
	}

	return s
}
