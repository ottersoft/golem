package semver

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var semverRx = regexp.MustCompile(`(?i)^\s*v?(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?\s*$`)

// Semver represents a semantic version.
type Semver struct {
	Major   int
	Minor   int
	Patch   int
	Release string
	Build   string
}

func (v Semver) String() string {
	s := fmt.Sprintf("v%d.%d.%d", v.Major, v.Minor, v.Patch)

	if v.Release != "" {
		s = fmt.Sprintf("%s-%s", s, v.Release)
	}

	if v.Build != "" {
		s = fmt.Sprintf("%s+%s", s, v.Build)
	}

	return s
}

// Bump increases the Patch version number by 1.
func (v Semver) Bump() Semver {
	v.Patch++
	return v
}

// BumpMinor increases the Minor version number by 1, and sets the Patch
// version number to 0.
func (v Semver) BumpMinor() Semver {
	v.Patch = 0
	v.Minor++
	return v
}

// BumpMajor increases the Major version number by 1, and sets the Minor and
// Patch version numbers to 0.
func (v Semver) BumpMajor() Semver {
	v.Patch = 0
	v.Minor = 0
	v.Major++
	return v
}

// Compare compares the receiver with the given version. Returns 0 if the
// versions are identical. Returns >= 1 if the receiver is greater than the
// given version. Returns <= 1 if the receiver is less than the given version.
func (v Semver) Compare(v1 Semver) int {
	if v.Major != v1.Major {
		return v.Major - v1.Major
	} else if v.Minor != v1.Minor {
		return v.Minor - v1.Minor
	} else if v.Patch != v1.Patch {
		return v.Patch - v1.Patch
	} else if v.Release != v1.Release {
		strings.Compare(v.Release, v1.Release)
	} else if v.Build != v1.Build {
		strings.Compare(v.Build, v1.Build)
	}

	return 0
}

// Parse a semver string into a Semver object. Ok will be false if the string
// was not a valid semver.
func Parse(s string) (v Semver, ok bool) {
	m := semverRx.FindStringSubmatch(s)

	if m == nil {
		return
	}

	ok = true
	v = Semver{
		Major:   atoi(m[1]),
		Minor:   atoi(m[2]),
		Patch:   atoi(m[3]),
		Release: m[4],
		Build:   m[5],
	}

	return
}

// MustParse parses a semver string into a Semver object. Panics if the string
// is not a valid semver string.
func MustParse(s string) Semver {
	v, ok := Parse(s)

	if !ok {
		panic(fmt.Errorf("Invalid semver: %s", v))
	}

	return v
}

func atoi(s string) int {
	i, err := strconv.Atoi(s)

	if err != nil {
		return 0
	}

	return int(i)
}
