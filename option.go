package golem

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var optionRx = regexp.MustCompile(`(?i)^\s*-{1,2}([a-z0-9-]+)(?:\s+<(bool|int|float|string)>)?\s+(.*?)\s*$`)

// Option is a wrapper around an option value extracted from command line
// arguments. It provides typed accessor methods (e.g. Bool(), Int(), Float(),
// and String()).
type Option struct {
	Value interface{}
}

// Bool gets the option as a boolean, performing a conversion if necessary.
func (o Option) Bool() bool {
	if v, ok := o.Value.(*bool); ok {
		return *v
	} else if v, ok := o.Value.(*int64); ok {
		return *v != 0
	} else if v, ok := o.Value.(*float64); ok {
		return *v != 0
	} else if v, ok := o.Value.(*string); ok {
		b, _ := strconv.ParseBool(*v)
		return b
	}

	return false
}

// Int gets the option as an int64, performing a conversion if necessary.
func (o Option) Int() int64 {
	if v, ok := o.Value.(*int64); ok {
		return *v
	} else if v, ok := o.Value.(*float64); ok {
		return int64(*v)
	} else if v, ok := o.Value.(*string); ok {
		i, _ := strconv.ParseInt(*v, 10, 64)
		return i
	} else if v, ok := o.Value.(*bool); ok {
		if *v {
			return 1
		}
		return 0
	}

	return 0
}

// Float gets the option as a float64, performing a conversion if necessary.
func (o Option) Float() float64 {
	if v, ok := o.Value.(*float64); ok {
		return *v
	} else if v, ok := o.Value.(*int64); ok {
		return float64(*v)
	} else if v, ok := o.Value.(*string); ok {
		i, _ := strconv.ParseFloat(*v, 64)
		return i
	} else if v, ok := o.Value.(*bool); ok {
		if *v {
			return 1
		}
		return 0
	}

	return 0
}

// String gets the option as a string, performing a conversion if necessary.
func (o Option) String() string {
	if v, ok := o.Value.(*string); ok {
		return *v
	} else if v, ok := o.Value.(*int64); ok {
		return fmt.Sprintf("%d", *v)
	} else if v, ok := o.Value.(*float64); ok {
		return fmt.Sprintf("%f", *v)
	} else if v, ok := o.Value.(*bool); ok {
		return fmt.Sprintf("%v", *v)
	}

	return ""
}

// OptionSet is a set of options parsed from command line arguments.
type OptionSet struct {
	v map[string]interface{}
}

// Option gets an Option instance by name.
func (o OptionSet) Option(name string) Option {
	v, ok := o.v[name]

	if ok {
		return Option{v}
	}

	return Option{}
}

// OptionSetFactory is a function which accepts command line arguments and
// returns an OptionSet.
type OptionSetFactory func(args []string) OptionSet

// ParseOptions accepts usage name and usage strings, and parses them into an
// OptionSetFactory.
func ParseOptions(name string, options []string) OptionSetFactory {
	f := flag.NewFlagSet(name, flag.ContinueOnError)
	v := map[string]interface{}{}
	d := []string{}
	hasOptions := false
	f.SetOutput(ioutil.Discard)

	for _, s := range options {
		m := optionRx.FindStringSubmatch(s)

		if m == nil {
			d = append(d, wrap(removeMultiSpaces(removeLineBreaks(s)), 80))
			continue
		}

		if m[1] == "help" {
			continue
		}

		switch m[2] {
		case "bool", "":
			v[m[1]] = f.Bool(m[1], false, m[3])
		case "int":
			v[m[1]] = f.Int64(m[1], 0, m[3])
		case "float":
			v[m[1]] = f.Float64(m[1], 0, m[3])
		case "string":
			v[m[1]] = f.String(m[1], "", m[3])
		default:
			continue
		}

		hasOptions = true
	}

	return func(args []string) OptionSet {
		err := f.Parse(args)

		if err != nil {
			if err == flag.ErrHelp {
				printUsage(os.Stderr, f, d, hasOptions)
			} else {
				var b strings.Builder
				printUsage(&b, f, d, hasOptions)
				panic(fmt.Errorf("%s\n%s", b.String(), err))
			}
		}

		return OptionSet{v}
	}
}

func printUsage(w io.Writer, f *flag.FlagSet, d []string, hasOptions bool) {
	fmt.Fprintf(w, "Usage: golem %s", f.Name())

	if hasOptions {
		fmt.Fprintf(w, " [options...]\n       golem %s --help\n", f.Name())
	} else {
		fmt.Fprintf(w, " [--help]\n")
	}

	for _, p := range d {
		fmt.Fprintf(w, "\n%s\n", p)
	}

	fmt.Fprintf(w, "\nOptions:\n")
	f.SetOutput(w)
	f.PrintDefaults()
	f.SetOutput(ioutil.Discard)
	fmt.Fprintf(w, "  -help\n        Print this usage text.\n")
}
