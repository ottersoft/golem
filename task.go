package golem

import (
	"os"
)

// TaskSpec is a runnable task + option strings.
type TaskSpec struct {
	fn      func(c Context)
	options []string
}

// Run the task function. The current working directory is saved before running
// and restored when the function is complete.
func (t TaskSpec) Run(s Context) {
	cwd, err := os.Getwd()

	if err != nil {
		panic(err)
	}

	defer func() {
		if err := os.Chdir(cwd); err != nil {
			panic(err)
		}
	}()

	t.fn(s)
}

// Options returns the task options string array.
func (t TaskSpec) Options() []string {
	return t.options
}
