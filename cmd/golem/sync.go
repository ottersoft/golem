package main

import (
	"fmt"

	"gitlab.com/ottersoft/golem"
	"gitlab.com/ottersoft/golem/semver"
)

func init() {
	if !golem.IsGolem() {
		return
	}

	golem.Task("sync", func(c golem.Context) {
		v := semver.MustParse(golem.Command("golem", "-version").OutputString())
		golem.Command("go", "get", "-u", fmt.Sprintf("gitlab.com/ottersoft/golem@%s", v)).Dir(".golem").Run()
		golem.Command("go", "mod", "tidy").Dir(".golem").Run()
	},
		"Set the Golem dependency version in '.golem/go.mod' file to match the installed Golem CLI version.",
	)
}
