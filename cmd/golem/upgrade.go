package main

import (
	"os"

	"gitlab.com/ottersoft/golem"
)

var tagsURL = "https://gitlab.com/api/v4/projects/17995018/repository/tags?order_by=name&sort=desc&search=^v"

type tag struct {
	Name string `json:"name"`
}

func init() {
	golem.Task("upgrade", func(c golem.Context) {
		home, err := os.UserHomeDir()

		if err != nil {
			panic(err)
		}

		var tags []tag
		r := c.RequestJSON("GET", tagsURL, nil, &tags)
		r.AssertSuccessResponseCode()

		if len(tags) == 0 {
			c.Warn("no golem tags found")
			return
		}

		tag := tags[0].Name
		cmd := golem.Command("go", "get", "-u", "gitlab.com/ottersoft/golem/cmd/golem@"+tag)
		cmd.Dir(home)
		cmd.Env("GO111MODULE", "on")
		cmd.Run()
	},
		"Install the latest version of the Golem CLI to the $GOBIN directory.",
	)
}
