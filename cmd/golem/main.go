package main

import (
	"os"
	"runtime"

	"gitlab.com/ottersoft/golem"
	"gitlab.com/ottersoft/golem/internal"
)

func main() {
	if os.Getenv("GOOS") == "" {
		os.Setenv("GOOS", runtime.GOOS)
	}
	if os.Getenv("GOARCH") == "" {
		os.Setenv("GOARCH", runtime.GOARCH)
	}

	if len(os.Args) >= 2 {
		if os.Args[1] == "init" || os.Args[1] == "upgrade" || os.Args[1] == "sync" {
			golem.Run()
			return
		} else if os.Args[1] == "-version" || os.Args[1] == "--version" {
			golem.Print(os.Stdout, internal.Version)
			return
		}
	}

	if s, err := os.Stat(".golem"); err != nil || !s.IsDir() {
		golem.Run()
		return
	}

	exitCode := 0

	defer func() {
		os.Exit(exitCode)
	}()
	defer func() {
		os.Remove(".golem/__golem_bin")
	}()

	if !build() || !run() {
		exitCode = 1
	}
}

func build() (ok bool) {
	defer func() {
		if err := recover(); err != nil {
			golem.Print(os.Stderr, golem.Style("Failed to build the '.golem' directory.", golem.ColorRed))
		}
	}()

	golem.Command("go", append([]string{"build", "-o=__golem_bin", "-tags=golem", "."})...).Dir(".golem").Run()
	return true
}

func run() (ok bool) {
	defer func() {
		recover()
	}()
	golem.Command(".golem/__golem_bin", os.Args[1:]...).Run()
	return true
}
