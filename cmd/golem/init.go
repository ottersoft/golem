package main

import (
	"strings"

	"gitlab.com/ottersoft/golem"
)

func init() {
	golem.Task("init", func(c golem.Context) {
		c.Log("ensuring .golem directory")
		c.EnsureDir(".golem")
		c.SetDir(".golem")

		if c.Exists("go.mod") {
			c.Log("go.mod exists")
		} else {
			c.Exec("go", "mod", "init", "golem")
		}

		if c.Exists("golem.go") {
			c.Log("golem.go exists")
		} else {
			c.Log("creating new golem.go")
			c.Write("golem.go", []byte(strings.TrimSpace(`
package main

import (
	"gitlab.com/ottersoft/golem"
)

func main() {
	// Define build tasks. Each task has a name and a callback function. The
	// callback function receives a Context object which contains task
	// information and utility funcions.

	golem.Task("build", func(c golem.Context) {
		c.RunTask("test")
		c.Exec("go", "mod", "tidy")
		c.EnsureDir("out")
		c.Exec("go", "build", "-o=out", "./...")
	})

	golem.Task("cover", func(c golem.Context) {
		c.RunTask("test")
		c.Exec("go", "tool", "cover", "-html=cp.out")
	})

	golem.Task("test", func(c golem.Context) {
		c.Exec("go", "test", "-coverprofile", "cp.out", "./...")
		c.Exec("go", "tool", "cover", "-func=cp.out")
	})

	golem.Task("clean", func(c golem.Context) {
		c.Delete("**/out/")
		c.Delete("**/*.out")
	})

	// The Run() function executes the command indicated by the first argument
	// passed to Golem on the command line. This method will ALWAYS exit, so it
	// should be the very last call in main().
	golem.Run()
}			
			`)+"\n"))
		}
	},
		"Create the '.golem' sub-module directory, which defines a basic set of build tasks in the '.golem/golem.go' file.",
	)
}
