package golem

import (
	"fmt"
	"io"
)

// StyleCode is an ASCII style code value: \033[<StyleCode>m
type StyleCode int

// ColorRed : ASCII bright red.
const ColorRed = StyleCode(91)

// ColorYellow : ASCII bright yellow.
const ColorYellow = StyleCode(93)

// ColorBlue : ASCII bright blue.
const ColorBlue = StyleCode(94)

// ColorGreen : ASCII bright grean.
const ColorGreen = StyleCode(92)

// ColorGray : ASCII bright gray.
const ColorGray = StyleCode(90)

// Style wraps a string in ASCII code escapes sequences.
func Style(s string, cs ...StyleCode) string {
	s = fmt.Sprintf("%s\033[0m", s)

	for _, c := range cs {
		s = fmt.Sprintf("\033[%dm%s", c, s)
	}

	return s
}

// Print is a shortcut for: fmt.Fprintf(writer, format + "\n", args...)
func Print(w io.Writer, f string, a ...interface{}) {
	fmt.Fprintf(w, f+"\n", a...)
}
