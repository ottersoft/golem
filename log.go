package golem

import "os"

// Log prints an informational message to stdout. No ASCII escapes will be
// applied to stdout messages.
func (l *context) Log(f string, a ...interface{}) {
	Print(os.Stdout, f, a...)
}

// Warn prints a warning message to stderr.
func (l *context) Warn(f string, a ...interface{}) {
	Print(os.Stderr, Style(f, ColorYellow), a...)
}
